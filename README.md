# Virtual World Interactive Visualization of Covid-19 Pandemic

In a virtual world user experience, interact with a virtual Earth to investigate the pandemic across the world.

This is a submission to the 2020 Planetwide SOS Hackathon https://soshackathon.com/

## Inspiration
Team member luliiana had a world-encompassing vision she has been developing of interactive visualization of networked data and AI analytics.
We worked together to pick a starting point to visualize the Covid-19 Pandemic Data, based on interaction with a virtual earth, to be implemented in Decentraland's virtual world.

## What it does
Allows you to see countries on the globe, and, by touching them, see the current demographic and statistical data about the pandemic in each country.

## How I built it
The Virtual World experience is built on the Decentraland.org platform, using Blender, Typescript, and the Decentraland SDK.  The data comes from the Johns Hopkins University Covid-19 international database API, plus in some cases latitude/longitude data from a public Google country database.

## Challenges I ran into
Initial attempts to use a more complete historical dataset were not successful because it was too large to consume.  Fortunately the JHU database was found, which met the requirements pretty well.
Also, determining good ways to represent the most important data visually on the globe was challenging, and for the prototype I settled on % death rate controlling a rainbow color spectrum of the touch points' emission colors
, from red being highest to violet being lowest, although some of the data on the low end is questionable so there is a lot of violet.  Also, some country pandemic data is incomplete in the JHU dataset, so I chose to use un-emissive dark items to represent these.

## Accomplishments that I'm proud of
Being able to create a very presentable working prototype in about 2 weeks and deploying it for the public to visit in Decentraland.

## What I learned
The data set is very large.  The other types of data we will want may be challenging to access.  Narrowing a broad vision into a focused prototype to demonstrate.

## What's next for Virtual World Interactive Visualization of Covid-19 Pandemic
Being able to see other factors, and to drill into the data, to connect this with grassroots user-generated data, such as data from user mobile apps, and to make useful tools for users, medical researchers and providers, and agencies.  The vision is to take this further so that one can drill down as a doctor or researcher toward more specific information, and also that an individual can submit data that ties into this at the grassroots level, but those extensions will not be completed by the hackathon deadline, of course.

