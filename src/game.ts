import {spawnGltfX} from './modules/SpawnerFunctions'
import {countryData, countryDataType} from './CountryData'
import {CovidManagerHUD, CovidManager_tilt, CovidManager_rotate } from './modules/CovidManagerHUD'
import {CFBasicTextSign} from './modules/CFBasicTextSign'
import {CFBasicTextOverlay} from './modules/CFBasicTextOverlay'
//import {BuilderHUD} from './modules/BuilderHUD';


log ("=====================================\n\
cc-earth 2020 06 14 1821 UTC live data, date, button labels, 1.5x size\n\
=====================================")


let covidManagerHudListener = new EventManager()

covidManagerHudListener.addListener(CovidManager_tilt, null, async ({bUp}) => {
  tiltEarth(bUp)
})

covidManagerHudListener.addListener(CovidManager_rotate, null, async ({bLeftwise}) => {
  rotateEarth(bLeftwise)
})    
let covidManagerHUD:CovidManagerHUD = new CovidManagerHUD(covidManagerHudListener)

function displayStarInfo(info:any) {

  let text:string = 
  '\n\
  COVID-19 INFO\n\
  \n\
  Country: '+info.name+'\n\
  Population: '+info.population+'\n\
  Covid-19 Cases: '+info.latest_data.confirmed+'\n\
  Covid-19 Deaths: '+info.latest_data.deaths+'\n\
  % Case Deaths: '+Math.floor(info.latest_data.calculated.death_rate*100+.5)/100+'%\n\
  Updated: '+info.updated_at.substring(0,10)+'\n\
  \n\
  Click nation dot for info.\n\
    Red = high death rate\n\
    Violet = low death rate\n\
    Black = incomplete data\n\
  \n\
  Press ESC to use this UI.'
  covidManagerHUD.setText(text)
}

class Star extends Entity {

  constructor (
      x:number, y:number, z:number, 
      xRot:number, yRot:number, zRot:number, 
      xScale:number, yScale:number, zScale:number,
      color:Color3,
      emissiveColor:Color3,
      emissiveIntensity:number,
      public info:any)
    {
    super()
    this.addComponent(new Transform({ position: new Vector3(x, y, z),rotation: Quaternion.Euler(xRot, yRot, zRot) ,scale: new Vector3(xScale, yScale, zScale)  }))
    this.addComponent(new BoxShape())
    let mtl = new Material()
    mtl.albedoColor = color
    mtl.emissiveColor = emissiveColor
    mtl.emissiveIntensity = emissiveIntensity
    this.addComponent(mtl)
    this.addComponent(new OnClick(() => {
      //TODO present information about this items
        //log("Show Star information", this.info)
        displayStarInfo(this.info) 
      },
      {
        //button: ActionButton.PRIMARY,
        //showFeedback: true,
        hoverText: this.info.name,
        //distance: 8,
      }
      )
    )

    // this.addComponent(
    //   new OnPointerDown(
    //     (e) => {
    //       log("myEntity clicked", e)
    //       log("Show Star information", this.info.name)
    //     },
    //     {
    //       button: ActionButton.PRIMARY,
    //       showFeedback: true,
    //       hoverText: this.info.name,
    //       distance: 8,
    //     }
    //   )
    // )

  }
}

const globeRadius = 0.5

let spectrum:Color3[] = 
[
  Color3.Red(),
  new Color3(1,.5,0), // orange
  Color3.Yellow(),
  Color3.Green(),
  Color3.Blue(),
  Color3.Purple()
]

function colorSelector(n:number){ // 0 to 1
  if (n >= 1) n = 0.99999
  let i:number = Math.floor(n*spectrum.length)
  return spectrum[i]
}

function rad (degrees:number):number { //convert degrees to radians
  return degrees/180 * Math.PI;
}

function PointOnSphere(radius:number, latitude:number,longitude:number):Vector3{ //lat long in geo coordinate degrees.
  return new Vector3(
    radius*Math.sin(rad(longitude))*Math.cos(rad(latitude)),
    radius*Math.sin(rad(latitude)),
    -radius*Math.cos(rad(longitude))*Math.cos(rad(latitude)),
  )
}

type LatLongStarData = {
  latitude:number, 
  longitude:number
  xRot:number,yRot:number,zRot:number,
  scale:number,
  color:Color3,
  emissiveColor:Color3,
  emissiveIntensity:number,
  info:any
}

let latLongStarData:LatLongStarData[] = []

let sdi = 0 // starDataIndex

function findCoordinates(code:string) {
  for (let d of countryData) {
    if (d.country == code) {
      if (d.latitude == null || d.longitude == null || (d.latitude == 0 && d.longitude == 0) ){
        return null
      }
      return {"latitude":d.latitude, "longitude":d.longitude}
    }
  }
  return null
}



let gimbal = new Entity()
gimbal.addComponentOrReplace(new Transform({ position: new Vector3(8, 1.3, 8), scale:new Vector3(1,1,1) }))
engine.addEntity(gimbal)


let globeCenter = new Entity()
globeCenter.addComponentOrReplace(new Transform({ position: new Vector3(0,0,0), rotation:Quaternion.Euler(0,-30,0), scale:new Vector3(2,2,2) }))
globeCenter.setParent(gimbal)
engine.addEntity(globeCenter)

let sw  = new Entity()
sw.addComponentOrReplace(new Transform({ position: new Vector3(0, 0, 0), scale:new Vector3(1,1,1) }))
engine.addEntity(sw)


class BuildingWall extends Entity {
  wallSign:Entity
  constructor(
    parent:Entity,
    mtl:Material,
    x:number, y:number, z:number, 
    xRot:number, yRot:number, zRot:number, 
    xScale:number, yScale:number, zScale:number, wallText:string="", wallTextColor?:Color3,wallTextFontSize:number=1)
  {
    super()
    this.addComponent(new PlaneShape())
    this.addComponentOrReplace(new Transform({ position: new Vector3(x,y,z), rotation:Quaternion.Euler(xRot,yRot,zRot), scale:new Vector3(xScale,yScale,zScale) }))
    this.addComponentOrReplace(mtl)
    this.setParent(parent)
    this.wallSign = new CFBasicTextOverlay(this,  0,0,.4, 0,180,0,  .5,.5,1, wallText, wallTextColor, wallTextFontSize)
  }
}

class ObservationBuilding extends Entity {
  floor:Entity
  northWall:Entity
  westWall:Entity
  eastWall:Entity
  southWall:Entity
  ceiling:Entity
  arrow:Entity
  entranceSign:CFBasicTextSign
  entranceTextShape:TextShape
  
  constructor(
    public bldgMtl:Material,public entranceMtl:Material, 
    public entranceText:string, public entranceTextColor:Color3, public entranceTextFontSize:number, 
    public wallText:string, public wallTextColor:Color3, public wallTextFontSize:number
  ){
    super()
    this.floor = new BuildingWall(this,this.bldgMtl, 8,0,8, 90,0,0, 16,16,1)    
    this.northWall = new BuildingWall(this,this.bldgMtl, 8,8,15.95, 0,0,0, 16,16,1, this.wallText,this.wallTextColor, this.wallTextFontSize)
    this.westWall = new BuildingWall(this,this.bldgMtl, 0.05,8,8, 0,-90,0, 16,16,1, this.wallText,this.wallTextColor, this.wallTextFontSize)
    this.eastWall = new BuildingWall(this,this.bldgMtl, 15.95,8,9, 0,90,0, 14,16,1, this.wallText,this.wallTextColor, this.wallTextFontSize)
    this.southWall = new BuildingWall(this,this.bldgMtl, 7,8,0.05, 0,180,0, 14,16,1, this.wallText,this.wallTextColor, this.wallTextFontSize)
    this.ceiling = new BuildingWall(this,this.bldgMtl, 8,15.9,8, -90,0,0, 16,16,1)
    this.arrow = spawnGltfX(new GLTFShape("models/arrow/Low_poly_arrow_38_simple_blue.glb"), 15.4,0,0.6, 90,0,45, 1,1,1)

    let entranceSignText = entranceText

    this.entranceSign = new CFBasicTextSign(bldg, 15,3,1,  0,-45,0,  2.828,1.2,.01,entranceSignText)
    this.entranceSign.setColor(new Color3(0,0.4,0))
    this.entranceSign.setFontColor(new Color3(1,1,1))
    this.entranceSign.setFontSize(2)

    let wallSignEast = new CFBasicTextOverlay(this.southWall,  0,0,.4, 0,180,0,  .5,.5,1, entranceSignText)
    wallSignEast.setFontColor(new Color3(1,1,1))
    wallSignEast.setFontSize(1)

    let wallSignSouth = new CFBasicTextOverlay(this.southWall,  0,0,.4, 0,180,0,  .5,.5,1, entranceSignText)
    wallSignSouth.setFontColor(new Color3(1,1,1))
    wallSignSouth.setFontSize(1)

    let wallSignWest = new CFBasicTextOverlay(this.westWall,  0,0,.4, 0,180,0,  .5,.5,1, entranceSignText)
    wallSignWest.setFontColor(new Color3(1,1,1))
    wallSignWest.setFontSize(1)

    let wallSignNorth = new CFBasicTextOverlay(this.northWall,  0,0,.4, 0,180,0,  .5,.5,1, entranceSignText)
    wallSignNorth.setFontColor(new Color3(1,1,1))
    wallSignNorth.setFontSize(1)

  }
}

let bldgMtl = new Material()
bldgMtl.albedoColor = Color3.Black()
bldgMtl.roughness = 1
bldgMtl.metallic = 0

let entranceSignMaterial = new Material()
entranceSignMaterial.albedoColor = new Color3(0,0.4,0)
entranceSignMaterial.roughness = 1
entranceSignMaterial.metallic = 0

let signText = "Covid-19\nVirtual Research Center"
let signTextColor = Color3.White()

let bldg = new ObservationBuilding(bldgMtl, entranceSignMaterial, signText, signTextColor, 1, signText, signTextColor, 1)
bldg.setParent(sw)

// let earth0 = new Entity()
// earth0.addComponent(new GLTFShape("models/Earth/globe_no_stand.gltf"))
// earth0.addComponentOrReplace(new Transform({ position: new Vector3(0,0,0), scale:new Vector3(.57,.57,.57) }))
// earth0.setParent(globeCenter)

let earth = new Entity()
earth.addComponent(new GLTFShape("models/Earth/globe_no_stand2.gltf"))
earth.addComponentOrReplace(new Transform({ position: new Vector3(0,0,0), rotation:Quaternion.Euler(0,197 ,0), scale:new Vector3(.57,.57,.57) }))
earth.setParent(globeCenter)

function rotateEarth(bLeftwise:boolean) {
  globeCenter.getComponent(Transform).rotate(Vector3.Up(), bLeftwise?15:-15)
}
function tiltEarth(bUp:boolean) {
  gimbal.getComponent(Transform).rotate(Vector3.Right(), bUp?15:-15)
}

///// Connect to the REST API

class RestService {
  host:string
  port:number
  apiBase:string

  /**
   * 
   * @param host
   * @param port
   * @param apiBase // Fixed part of the API endpoint, start with slash.  Don't end with a slash.  e.g. /api/service
   */
  constructor(host:string, port:number, apiBase:string){
    this.host = host
    this.port = port
    this.apiBase = apiBase
  }

  /**
   * 
   * @param apiEndpoint  // If there is anything more then apiBase, start with slash, no slash at end.  e.g. /finalendpoint
   *                     // Can be emtpy string. 
   * @param body // e.g. { x: x, y: y, color: color }
   */
  async postDataToService(apiEndpoint:string, body:any){
    let url:string = this.host+(this.port==null?"":":"+this.port)+this.apiBase+apiEndpoint
    let method = 'POST'
    let headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
    let response:Response
    let json:JSON

    //executeTask(async () => {
      //log ("POST.body = ",JSON.stringify(body))
      try {
        response = await fetch(url, {
          headers: headers,
          method: method,
          body: JSON.stringify(body)
        })
        json = await response.json()
      } catch {
        log('postDataToService error ', response.status, response.statusText)
      }
    //})
    // return JSON.parse(response.text())
    // return {status:response.status, statusText:response.statusText}
    //return json
  }

  
  /**
   * 
   * @param apiEndpoint  // If there is anything more then apiBase, start with slash, no slash at end.  e.g. /finalendpoint
   *                     // Can be emtpy string. 
   * @param body // e.g. { x: x, y: y, color: color }
   */
  async getDataFromService(apiEndpoint:string, body:string):Promise<JSON>{
    //let url:string = this.host+":"+this.port+this.apiBase+apiEndpoint
    let url:string = this.host+(this.port==null?"":":"+this.port)+this.apiBase+apiEndpoint
    //log ("getDataFromService url =",url)
    // These options seem not to be needed for GET
    //let method = 'GET'
    //let headers = { 'Content-Type': 'application/json' }
    let json:JSON = null //TODO handle error better.
    let response:Response

    //executeTask(async () => { // executeTask was delaying things unnecessarily 
      try {
        response = await fetch(url)
        json = await response.json()
        // todo turn sync indicator = success
      } catch (e) {
        log('getDatafromService error ', e.toString) // TODO turn sync indicator = fail
      }
    //})
    return json
  }
}


let jhuDataServiceUrl:string = "https://corona-api.com"
let jhuDataServicePort:number = null
let jhuDataServiceApi:string = "/countries"
const jhuDataService = new RestService(jhuDataServiceUrl, jhuDataServicePort, jhuDataServiceApi)

let stars:Star[] = []

let i = 0

/**
 * 
 * @param json // set all the state to what was gotten from server
 */
function processPolledState(json){
  //log ("ProcessPolledState: json =",json)
  for (let cd of json.data) {
    if (cd.code == null || cd.name == null){
      log("missing code or name data from ",cd)
    }
    else {
      if (cd.coordinates == null || cd.coordinates.latitude == null || cd.coordinates.longitude == null || (cd.coordinates.latitude == 0 && cd.coordinates.longitude == 0)) {
          //log("looking up missing coordinates for ", cd.code)
          cd.coordinates = findCoordinates(cd.code)
      }
      if (cd.coordinates !== null) {
        //let color = colorSelector(Math.random())
        let dr = cd.latest_data.calculated.death_rate
        
        let dri = 1-dr/15 // 15 -> 0  0 -> 1
        if (dri<0) dri = 0
        if (dri>1) dri = 1
        let color = colorSelector(dri)
        latLongStarData[sdi] = {
          latitude:cd.coordinates.latitude,
          longitude:cd.coordinates.longitude,
          xRot:0,yRot:0,zRot:0,
          scale: .012*globeRadius,
          //scale: Math.random()/30 +.01,
          color:Color3.Black(),
          emissiveColor:color,
          //emissiveIntensity:Math.random(),
          emissiveIntensity: ((dr==null||dr==0)?0:5),
          info: cd
        }
        //log (cd.code,cd.name, dr, dri) //TODO revisit color and emission
        sdi++ 
      }
    }
  }
  for (let sd of latLongStarData) {
    let cartesianCoords:Vector3 = PointOnSphere(globeRadius*1.008,sd.latitude,sd.longitude) // 6m radius
    let x = cartesianCoords.x
    let y = cartesianCoords.y
    let z = cartesianCoords.z
    stars[i] = new Star (x, y, z, sd.xRot, sd.yRot, sd.zRot, sd.scale, sd.scale, sd.scale, sd.color,sd.emissiveColor,sd.emissiveIntensity, sd.info)
    //log(x,y,z)
    stars[i].setParent(globeCenter)
    //engine.addEntity(stars[i])
    i++
  }
}

async function getDataFromService() {
  let json = await jhuDataService.getDataFromService("", "") //TODO fix the params here.
  if (json != null) {
    processPolledState(json)
  }
  else {
    log ("jhuData is null")
  }
}

getDataFromService()

// let builderHUD = new BuilderHUD()
// builderHUD.setDefaultParent(bldg)
// builderHUD.attachToEntity(gimbal)