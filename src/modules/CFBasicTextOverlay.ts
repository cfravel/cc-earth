export class CFBasicTextOverlay extends Entity {
  textShape:TextShape
  constructor(parent:Entity,x:number, y:number, z:number, xRot:number, yRot:number, zRot:number, xScale:number, yScale:number, zScale:number, text:string, textColor:Color3=Color3.Black(), textFontSize:number=1){
      super()
      let transform = new Transform({position:new Vector3(x,y,z), scale: new Vector3(xScale,yScale,zScale), rotation: Quaternion.Euler(xRot,yRot,zRot)})
      this.addComponentOrReplace(transform)

      this.textShape = new TextShape()
      this.addComponent(this.textShape)
      this.textShape.value = text
      this.textShape.fontSize = textFontSize
      this.textShape.color = textColor
      if (parent != null){
          this.setParent(parent)
      }
  }
  setText(text:string){
      this.textShape.value = text
  }
  setFontSize(fontSize:number){
      this.textShape.fontSize = fontSize
  }
  setFontColor(color:Color3){
      this.textShape.color = color
  }
  // setFontFamily(family:string){
  //     this.textShape.fontFamily = family
  // }
  setFontOpacity(opacity:number){
      this.textShape.opacity = opacity
  }
  setFontWeight(weight:string){ // can be normal, bold, bolder, or lighter
      this.textShape.fontWeight = weight
  }
}