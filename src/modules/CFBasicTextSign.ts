export {CFBasicTextSign}

class CFBasicTextSign extends Entity {
    panel:Entity
    panelShape:BoxShape
    backPanel:Entity
    panelMaterial:Material
    textShape:TextShape
    text:Entity
    constructor(parent:Entity,x:number, y:number, z:number, xRot:number, yRot:number, zRot:number, xScale:number, yScale:number, zScale:number, text:string="",panelMaterial:Material=null, textColor=Color3.Black(), textFontSize:number=2.2 ){
        super()
        let transform = new Transform({position:new Vector3(x,y,z), rotation: Quaternion.Euler(xRot,yRot,zRot)})
        this.addComponentOrReplace(transform)
        engine.addEntity(this)

        this.panel = new Entity()
        this.panel.setParent(this)
        this.panelShape = new BoxShape()
        this.panel.addComponent(this.panelShape)
        let transformP = new Transform({scale:new Vector3(xScale,yScale,zScale)})
        this.panel.addComponentOrReplace(transformP)
        if (panelMaterial!==null) {
            this.panelMaterial = panelMaterial
        }
        else {
            this.panelMaterial = new Material()
            this.panelMaterial.albedoColor = Color3.White()
        }
        this.panel.addComponent(this.panelMaterial)

        this.text = new Entity()
        this.text.setParent(this)
        let transformT = new Transform({position:new Vector3(0,0,-zScale/2-.005)}) // put text just in front of panel
        this.text.addComponentOrReplace(transformT)

        this.textShape = new TextShape()
        this.text.addComponent(this.textShape)
        this.textShape.value = text
        this.textShape.fontSize = 2.2
        this.textShape.color = new Color3(0,0,0)
        if (parent != null){
            this.setParent(parent)
        }
    }
    setText(text:string){
        this.textShape.value = text
    }
    setFontSize(fontSize:number){
        this.textShape.fontSize = fontSize
    }
    setFontColor(color:Color3){
        this.textShape.color = color
    }
    // setFontFamily(family:string){
    //     this.textShape.fontFamily = family
    // }
    setFontOpacity(opacity:number){
        this.textShape.opacity = opacity
    }
    setFontWeight(weight:string){ // can be normal, bold, bolder, or lighter
        this.textShape.fontWeight = weight
    }

    setMaterial(material:Material){
        this.panelMaterial = material
        this.panel.addComponentOrReplace(this.panelMaterial)
    }
    setColor(color:Color3){
        this.panelMaterial.albedoColor = color
    }
}


